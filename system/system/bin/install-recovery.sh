#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:2d76dffd813d1bc7362cc02955a7c7c10b95703e; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:91eab75092f8e738e12482b927645136aff2f1e1 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:2d76dffd813d1bc7362cc02955a7c7c10b95703e && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
